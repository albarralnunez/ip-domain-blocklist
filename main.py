import asyncio
import aiohttp
from async_timeout import timeout
import shutil


async def fetch(session, url, num_urls):
    async with session.get(url) as response:
        if response.status != 200:
             return ''
        return url

async def get_url(session, url, num_urls):
    try:
        return await fetch(session, url, num_urls)
    except aiohttp.client_exceptions.ClientConnectorError:
        return ''


async def fetch_all(session, urls, loop):
    results = await asyncio.gather(*[loop.create_task(get_url(session, url, len(urls)))
                                   for url in urls])
    return results

async def main(loop):
    with open('block_sources.txt.backup', 'r') as f:
        urls_with_repetitions = [x.strip() for x in f.readlines()]
    print(f"URL's with repetitions {len(urls_with_repetitions)}")
    urls = set(urls_with_repetitions)
    print(f"URL's without repetitions {len(urls)}")
    async with  aiohttp.ClientSession(loop=loop) as session:
        valid_urls_with_blanks = await fetch_all(session, urls, loop)
        valid_urls = list(filter(bool, valid_urls_with_blanks))
        print(f"Valid URL's {len(valid_urls)}")
    with open('block_sources.txt', 'w') as f:
        f.write("\n".join(valid_urls))

if __name__ == "__main__":
    print('Start')
    shutil.copy('block_sources.txt', 'block_sources.txt.backup')
    try:
        loop = asyncio.get_event_loop()
        loop.run_until_complete(main(loop))
    except Exception:
        shutil.move('block_sources.txt.backup', 'block_sources.txt')
        raise
    print('Finished')
